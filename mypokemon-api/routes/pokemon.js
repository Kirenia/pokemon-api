const express= require('express');
const router = express.Router();
//const {getPokemon} = require('../models/pokemonModels');
const Pokemon = require('../models/pokemonModels');


//router.get() Method

router.get('/', (req, res) => {
  const apiResp = Pokemon.getPokemon();
    res.json(apiResp).end();
});


//Add a Pokemon route
router.post('/add', (req, res) => {
    const apiResp = Pokemon.addPokemon()
    res.json(apiResp).end();
});
router.put('/update', (req, res) => {
    const apiResp = Pokemon.updatePokemon();
    res.json(apiResp).end();
});

router.delete('/delete', (req, res) => {
    const apiResp = Pokemon.deletePokemon();
    res.json(apiResp).end();
});

module.exports= router;