const ApiResponse = require('./api-response');

class pokemonModels {

    getPokemon() {

        const apiResp = new ApiResponse();
       apiResp.setData( ['poke1', 'pichachu', 'pica', 'pikpoke']);
        
        return apiResp;


    }

    addPokemon() {

        const apiResp = new ApiResponse();
        apiResp.setData('Posted');
      
        return apiResp;
    }

    updatePokemon(){

        const apiResp= new ApiResponse();
        apiResp.setData('updated');
        return apiResp;
    }

    deletePokemon(){

        const apiResp= new ApiResponse();
        apiResp.setData('deleted');
        return apiResp;
    }

}

module.exports = new pokemonModels();