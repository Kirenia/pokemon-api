const express =  require('express');
const app= express();
const port = process.env.PORT || 3000;

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended:false}));
app.use((req,res,next)=>{
    let authenticated= true;
    if (authenticated){
        next();
    }
    else{

        res.json({error: 'Not autenticated'});
    }

});
//Import Pokemon
const pokemonRoutes= require('./routes/pokemon.js');
app.use('/api/v1/pokemon',pokemonRoutes);
//app.use('/pokemon',pokemonRoutes);


app.listen(port,()=>{
    console.log(`server running in ${port}`);
});